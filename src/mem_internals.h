#ifndef MEMORY_OLOLOCATOR_MEM_INTERNALS_H_
#define MEMORY_OLOLOCATOR_MEM_INTERNALS_H_

#include <stddef.h>
#include <stdbool.h>
#include <inttypes.h>

#define REGION_MIN_SIZE (2 * 4096)

struct Region {
    void* address;
    size_t size;
    bool extends;
};

static const struct Region REGION_INVALID = {0};

inline bool isRegionInvalid(const struct Region* r) {
    return r->address == NULL;
}

typedef struct {size_t bytes;} BlockCapacity;
typedef struct {size_t bytes;} BlockSize;

struct BlockHeader {
  struct BlockHeader* next;
  BlockCapacity capacity;
  bool isFree;
  uint8_t contents[];
};

inline BlockSize sizeFromCapacity(BlockCapacity capacity) {
    return (BlockSize) {capacity.bytes + offsetof(struct BlockHeader, contents)};
}

inline BlockCapacity capacityFromSize(BlockSize size) {
    return (BlockCapacity) {size.bytes - offsetof(struct BlockHeader, contents)};
}

#endif //MEMORY_OLOLOCATOR_MEM_INTERNALS_H_