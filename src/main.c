#include <stdio.h>
#include <string.h>
#include "mem_internals.h"
#include "mem.h"
#include "mem_debug.h"

int main() {
    void* heap = initHeap(20000);

    void *block1 = myMalloc(100);
    void *block2 = myMalloc(100);
    void *block3 = myMalloc(20);
    debugHeap(stdout, heap);

    printf("\nDeleting third block of memory.\n");
    myFree(block3);
    debugHeap(stdout, heap);

    printf("\nDeleting first and second blocks of memory.\n");
    myFree(block2);
    myFree(block1);
    debugHeap(stdout, heap);

    printf("\nNot enough memory. New region extends the old one.\n");
    myMalloc(200324);
    myMalloc(1000);
    debugHeap(stdout, heap);

    return 0;
}

