#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

void debugBlock(struct BlockHeader* b, const char* fmt, ...);
void debug(const char* fmt, ...);

extern inline BlockSize sizeFromCapacity(BlockCapacity capacity);
extern inline BlockCapacity capacityFromSize(BlockSize size);

static bool isBlockBigEnough(size_t query, struct BlockHeader* block) {
    return block->capacity.bytes >= query;
}
static size_t pagesCount(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}
static size_t roundPages(size_t mem) {
    return getpagesize() * pagesCount(mem);
}

static void initBlock(void* address, BlockSize size, void* next) {
    *((struct BlockHeader*)address) = (struct BlockHeader)
            {
            .next = next,
            .capacity = capacityFromSize(size),
            .isFree = true
            };

}

static size_t regionActualSize(size_t query) {
    return maxSize(roundPages(query), REGION_MIN_SIZE);
}

extern inline bool isRegionInvalid(const struct Region* r);

static void* mapPages(void const* address, size_t length, int additionalFlags) {
    return mmap((void*) address, length, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS | additionalFlags, 0, 0 );
}

static struct Region allocateNewRegion(void const* address, size_t query) {
    struct Region region;
    query = regionActualSize(query);
    void* nextAddress = mapPages(address, query, MAP_FIXED_NOREPLACE);
    if (nextAddress == MAP_FAILED) {
        nextAddress = mapPages(address, query, 0);
        region = (struct Region) {nextAddress, query, false};
    } else
        region = (struct Region) {nextAddress, query, true};
    initBlock(nextAddress, (BlockSize) {.bytes = query}, NULL);
    return region;
}

static void* getBlockAfterCurrent(struct BlockHeader const* block) {
    return  (void*) (block->contents + block->capacity.bytes);
}

void* initHeap(size_t initialSize) {
    const struct Region region = allocateNewRegion(HEAP_START, initialSize);
    if (isRegionInvalid(&region)) return NULL;
    return region.address;
}

static bool isBlockSplittable(struct BlockHeader* block, size_t query) {
    return block-> isFree && query + offsetof(struct BlockHeader, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool splitBlock(struct BlockHeader* block, size_t query) {
    if (isBlockSplittable(block, query)) {
        struct BlockHeader* newBlock = (struct BlockHeader*) (block->contents + query);
        BlockSize size = (BlockSize){.bytes = block->capacity.bytes - query};
        initBlock(newBlock, size, block->next);
        block->capacity.bytes = query;
        block->next = newBlock;
        return true;
    }
    return false;
}

static bool isBlockContinuePrevious(struct BlockHeader const* current, struct BlockHeader const* next) {
    return (void*)next == getBlockAfterCurrent(current);
}

static bool isBlocksMergeable(struct BlockHeader const* current, struct BlockHeader const* next) {
    return current->isFree && next->isFree && isBlockContinuePrevious(current, next) ;
}

static bool mergeWithNextBlock(struct BlockHeader* block) {
    struct BlockHeader* next = block->next;
    if (isBlocksMergeable(block, next) && block-> next) {
        block->next = next->next;
        block->capacity.bytes = block->capacity.bytes + offsetof(struct BlockHeader, contents) + next->capacity.bytes;
        return true;
    }
    return false;
}

struct BlockSearchResult {
    enum {GOOD_BLOCK, BAD_BLOCK} type;
    struct BlockHeader* block;
};

static struct BlockSearchResult findOptimalBlock(struct BlockHeader* block, size_t size) {
    struct BlockHeader* currentBlock = block;
    while (currentBlock->next) {
        if (currentBlock->isFree && isBlockBigEnough(size, currentBlock)) {
            return (struct BlockSearchResult)
                    { .type = GOOD_BLOCK,
                      .block = currentBlock
                    };
        }
        if(!mergeWithNextBlock(currentBlock) && !(currentBlock->isFree)){
            currentBlock = currentBlock->next;
        }
    }
    if (currentBlock->isFree && isBlockBigEnough(size, currentBlock)) {
        return (struct BlockSearchResult) { .type = GOOD_BLOCK, .block = currentBlock };
    }
    return (struct BlockSearchResult) { .type = BAD_BLOCK, .block = currentBlock };
}

static struct BlockSearchResult memAllocBlock(size_t query, struct BlockHeader* block) {
    struct BlockSearchResult goodOrLast = findOptimalBlock(block, query);
    if (goodOrLast.type == GOOD_BLOCK) {
        splitBlock(goodOrLast.block, query);
    }
    return goodOrLast;
}

static struct BlockHeader* increaseHeap(struct BlockHeader*  last, size_t query) {
    query += offsetof(struct BlockHeader, contents);
    void* address = last->contents + last->capacity.bytes;
    const struct Region region = allocateNewRegion(address, query);
    if (isRegionInvalid(&region)) {
        return NULL;
    }

    last->next = (struct BlockHeader*) region.address;
    mergeWithNextBlock(last);

    return region.address;
}

static struct BlockHeader* memAlloc(size_t query, struct BlockHeader* const heap_start) {

    if (query < BLOCK_MIN_CAPACITY) {
        query = BLOCK_MIN_CAPACITY;
    }

    struct BlockSearchResult optimalBlock = memAllocBlock(query, heap_start);
    if (optimalBlock.type != GOOD_BLOCK){
        if (!increaseHeap(optimalBlock.block, query)) {
            return NULL;
        }
        optimalBlock = findOptimalBlock(optimalBlock.block, query);
    }

    optimalBlock.block->isFree = false;
    return optimalBlock.block;
}

void* myMalloc(size_t query) {
    struct BlockHeader* const address = memAlloc(query, (struct BlockHeader*) HEAP_START);
    if (address) return address->contents;
    else return NULL;
}

void myFree(void* mem) {
    struct BlockHeader* header = (struct BlockHeader*)((char*)mem - (char*)offsetof(struct BlockHeader, contents));
    header->isFree = true;
    while (header->next && header->next->isFree) {
        if(!mergeWithNextBlock(header))
            break;
    }
}