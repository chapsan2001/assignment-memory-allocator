#ifndef MEMORY_OLOLOCATOR_MEM_DEBUG_H_
#define MEMORY_OLOLOCATOR_MEM_DEBUG_H_
#include "mem_internals.h"

void debugStructInfo(FILE* f, void const* address);
void debugHeap(FILE* f, void const* ptr);
void debugBlock(struct BlockHeader* b, const char* fmt, ...);
void debug(const char* fmt, ...);

#endif //MEMORY_OLOLOCATOR_MEM_DEBUG_H_