#ifndef MEMORY_OLOLOCATOR_MEM_H_
#define MEMORY_OLOLOCATOR_MEM_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include <sys/mman.h>

#define HEAP_START ((void*)0x04040000)

void* myMalloc(size_t query);
void  myFree(void* mem);
void* initHeap(size_t initialSize);

#define DEBUG_FIRST_BYTES 4

void debugStructInfo(FILE* f, void const* address);
void debugHeap(FILE* f,  void const* ptr);

#endif //MEMORY_OLOLOCATOR_MEM_H_