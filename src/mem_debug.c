#include <stdio.h>
#include <stdarg.h>
#include "mem_internals.h"
#include "mem.h"

void debugStructInfo(FILE* f, void const* address) {
    struct BlockHeader const* header = address;
    fprintf(f,
        "%10p %10zu %8s   ",
        address,
        header->capacity.bytes,
        header->isFree ? "free" : "taken"
        );
    for (size_t i = 0; i < DEBUG_FIRST_BYTES && i < header->capacity.bytes; ++i)
        fprintf(f, "%hhX", header->contents[i]);
    fprintf(f, "\n");
}

void debugHeap(FILE* f,  void const* ptr) {
  fprintf(f, " --- Heap ---\n");
  fprintf(f, "%10s %10s %8s %10s\n", "start", "capacity", "status", "contents");
  for(struct BlockHeader const* header =  ptr; header; header = header->next)
    debugStructInfo(f, header);
}

void debugBlock(struct BlockHeader* b, const char* fmt, ...) {
  #ifdef DEBUG

  va_list args;
  va_start(args, fmt);
  vfprintf(stderr, fmt, args);
  debugStructInfo(stderr, b);
  va_end(args);

  #else
  (void) b; (void) fmt;
  #endif
}

void debug(const char* fmt, ...) {
#ifdef DEBUG

    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);

#else
    (void)fmt;
#endif
}