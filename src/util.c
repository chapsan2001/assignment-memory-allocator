#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "util.h"

_Noreturn void err( const char* message, ... ) {
  va_list args;
  va_start(args, message);
  vfprintf(stderr, message, args);
  va_end(args);
  abort();
}

extern inline size_t maxSize(size_t x, size_t y);