#ifndef MEMORY_OLOLOCATOR_UTIL_H_
#define MEMORY_OLOLOCATOR_UTIL_H_

#include <stddef.h>

inline size_t maxSize(size_t x, size_t y) {
	return (x >= y) ? x : y ;
}

_Noreturn void err(const char* message, ...);

#endif //MEMORY_OLOLOCATOR_UTIL_H_